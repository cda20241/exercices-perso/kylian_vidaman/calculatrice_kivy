from kivy.app import App
from kivy.graphics import Canvas, Color, Rectangle
from kivy.properties import ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget


class NotreAppli(App):
    def build(self):
        self.box1 = BoxLayout()
        self.box1.rows = 2

        self.gridGauche = GridLayout(rows=2)
        self.gridDroite = GridLayout(cols=2)

        self.label = Label(text="Entrez du texte")
        self.gridGauche.add_widget(self.label)

        self.input = TextInput()
        self.input.bind(text=self.text)
        self.gridGauche.add_widget(self.input)

        # alignement des boutons en vertical
        self.gridDroite.size_hint_y = 0.9
        # Marges intérieures des 2 grids
        self.gridGauche.padding = 100
        self.gridDroite.padding = 20
        # espacement entre les boutons
        self.gridDroite.spacing = 30

        # instanciation des 5 boutons
        self.btn_Rouge = self.bouton("Rouge", "red")
        self.btn_Bleu = self.bouton("Bleu", "blue")
        self.btn_Jaune = self.bouton("Jaune", "yellow")
        self.btn_Rose = self.bouton("Rose", "pink")
        self.btn_Marron = self.bouton("Marron", "brown")
        self.btn_Vert = self.bouton("Vert", "green")

        self.box1.add_widget(self.gridGauche)
        self.box1.add_widget(self.gridDroite)

        return self.box1

    """
    Fonction qui change la couleur du text du label
    """

    def changeCouleur(self, button):
        self.label.color = button.background_color

    """
    Fonction qui affecte au label la valeur du textInput
    """

    def text(self, instance, args):
        self.label.text = instance.text

    """
    Fonction qui créer un bouton avec du texte et une couleur de font, le bind a changeCouleur et l'ajoute au gridDroite
    """

    def bouton(self, texte, couleur):
        self.button = Button(text=texte, background_color=couleur, background_normal="")
        self.button.bind(on_press=self.changeCouleur)
        self.gridDroite.add_widget(self.button)


class InterfaceButton(GridLayout):
    def build(self, calculatrice_instance):
        self.cols = 4
        self.rows = 5
        self.size_hint = (1.0, 1.0)
        self.orientation = "lr-tb"
        self.Liste_interface_btn = [
            "%",
            "CE",
            "C",
            "/",
            "7",
            "8",
            "9",
            "*",
            "4",
            "5",
            "6",
            "-",
            "1",
            "2",
            "3",
            "+",
            "+/-",
            "0",
            ",",
            "=",
        ]
        self.Liste_id_btn = [
            "pourcentage",
            "clear",
            "clearAll",
            "division",
            "7",
            "8",
            "9",
            "multiplication",
            "4",
            "5",
            "6",
            "moins",
            "1",
            "2",
            "3",
            "plus",
            "negate",
            "0",
            "virgule",
            "resultat",
        ]
        self.calculatrice = calculatrice_instance
        self.My_Button()

    def My_Button(self):
        self.Liste_btn = []

        for i in range(0, len(self.Liste_interface_btn)):
            self.Liste_btn.append(Button())
            self.Liste_btn[i].text = self.Liste_interface_btn[i]
            self.Liste_btn[i].font_size = 20
            self.Liste_btn[i].id = self.Liste_id_btn[i]
            self.Liste_btn[i].bind(on_press=self.funct_btn)
            self.add_widget(self.Liste_btn[i])

    def funct_btn(self, instance):
        label = self.calculatrice.label
        if label.text == "Entrez votre calcul.":
            label.text = " "
        if instance.id == "resultat":
            label.text = str(eval(label.text))
        elif instance.id == "clear":
            label.text = label.text[:-1]
        elif instance.id == "clearAll":
            label.text = "Entrez votre calcul."
        elif instance.id == "negate":
            if label.text[0] != '-' :
                label.text = '-' + label.text
            else : 
                label.text = label.text[1:]
        else:
            label.text += instance.text


class Calculatrice(App):
    def build(self):
        self.calc = BoxLayout(orientation="vertical")
        self.gridTop = GridLayout(cols=1, size_hint=(1.0, 0.4))

        # interface lecture
        self.relatLayout = RelativeLayout()
        self.img = Image(source="", color=(1, 1, 1))
        self.relatLayout.add_widget(self.img)
        self.label = Label(
            text="Entrez votre calcul.",
            color=(0, 0, 0),
            font_size=20,
            size_hint=(0.95, 1.0),
            halign="right",
            valign="center",
        )
        self.label.bind(size=self.label.setter("text_size"))

        self.relatLayout.add_widget(self.label)
        self.gridTop.add_widget(self.relatLayout)

        # instanciation des 16 boutons
        self.all_btn = InterfaceButton()
        self.all_btn.build(calculatrice_instance=self)

        self.calc.add_widget(self.gridTop)
        self.calc.add_widget(self.all_btn)

        return self.calc


if __name__ == "__main__":
    Calculatrice().run()
